# Essentials of Linux System Administration LFS201

Essentials of Linux System Administration (LFS201).

## Getting started

This project it just to document my learning course [**Essentials of Linux System Administration (LFS201)**](https://training.linuxfoundation.org/certification/essentials-of-linux-system-administration-lfs201-lfcs/) and **LFCS Exam**.

**Useful resources:**

- [Training portal](https://trainingportal.linuxfoundation.org/learn/)
- [LFS-201 Forum Class](https://forum.linuxfoundation.org/categories/lfs201-class-forum)
- [Course Resources](https://training.linuxfoundation.org/cm/LFS201/)

## Linux Filesystem

1. FHS Linux Standard Directory Tree. [Filesystem Hierarchy Standard](https://refspecs.linuxfoundation.org/FHS_3.0/fhs-3.0.pdf)

**Main Directories**

1. `/`	Primary directory of the entire filesystem hierarchy.
1. `/bin`	Essential executable programs that must be available in single user mode.
1. `/boot`	Files needed to boot the system, such as the kernel, initrd or initramfs images, and boot configuration files and bootloader programs.
1. `/dev`	Device Nodes, used to interact with hardware and software devices.
1. `/etc`	System-wide configuration files.
1. `/home`	User home directories, including personal settings, files, etc.
1. `/lib`	Libraries required by executable binaries in /bin and /sbin.
1. `/lib64`	64-bit libraries required by executable binaries in /bin and /sbin, for systems which can run both 32-bit and 64-bit programs.
1. `/media`	Mount points for removable media such as CDs, DVDs, USB sticks, etc.
1. `/mnt`	Temporarily mounted filesystems.
1. `/opt`	Optional application software packages.
1. `/proc`	Virtual pseudo-filesystem giving information about the system and processes running on it. Can be used to alter system parameters.
1. `/run`	Run-time variable data, containing information describing the system since it was booted. Replaces the older /var/run.
1. `/sys`	Virtual pseudo-filesystem giving information about the system and processes running on it. Can be used to alter system parameters. Similar to a device tree and is part of the Unified Device Model.
1. `/root`	Home directory for the root user.
1. `/sbin`	Essential system binaries.
1. `/srv`	Site-specific data served up by the system. Seldom used.
1. `/tmp`	Temporary files; on many distributions lost across a reboot and may be a ramdisk in memory.
1. `/usr`	Multi-user applications, utilities and data; theoretically read-only.
1. `/var`	Variable data that changes during system operation.

